import click
import os
from .app import app, db, mkpath
from .models import Joueur, Session, Creneau, Adresse, Jeu, Message
import datetime
from werkzeug.security import generate_password_hash, check_password_hash

@app.cli.command()
def syncdb():
    '''Creer toutes les tables manquantes'''
    # creation de toutes les tables
    db.create_all()


@app.cli.command()
def loaddb():
    '''Ajoute les éléments à la bd '''
    if os.path.exists(mkpath("./donnees.db")):
        os.remove(mkpath("./donnees.db"))
    else:
        print("Fichier pas là")
    db.create_all()


    # --------------------------> JEUX <--------------------------#

    terraforming_mars = Jeu(titre="Terraforming Mars",
                            url_img="https://encrypted-tbn0.gstatic.com/images?q=tbn=ANd9GcRFydfF1SINTYntHcZ2CNrTCpPtnJ0vslgrww&usqp=CAU",
                            auteur="Jacob Fryxelius",
                            editeur="Intrafin",
                            annee_edition=2016,
                            nb_min_joueurs=1,
                            nb_max_joueurs=5,
                            duree_moyenne=120,
                            type_jeu="Stratégie SF, draft",
                            complexite="Expert",
                            age_min=12,
                            resume="L'ère de la domestication de Mars a commencé. Dans Terraforming Mars, de puissantes corporations travaillent pour rendre la Planète Rouge habitable. La température, l'oxygène et les océans sont les trois axes de développement principaux. Mais pour triompher, il faudra aussi construire des infrastructures pour les générations futures."
                            )

    db.session.add(terraforming_mars)

    loup_garou = Jeu(titre="Loup-Garou pour un Crépuscule",
                     url_img=" https://cdn2.philibertnet.com/360962-large_default/loup-garou-pour-un-crepuscule.jpg",
                     auteur="Ted Alspach, Akihisa Okui",
                     editeur="Ravensburger",
                     annee_edition=2016,
                     nb_min_joueurs=3,
                     nb_max_joueurs=10,
                     duree_moyenne=10,
                     type_jeu="Stragégie, trahison",
                     complexite="Famille",
                     age_min=3,
                     resume="Plus de personnages, plus d'interactions ! Chaque joueur joue le rôle d'un Villageois, d'un Loup-garou ou d'un personnage spécial. C'est à vous de deviner qui sont les Loups-garous et de tuer au moins l'un d'eux pour gagner... à moins que vous ne soyez vous-même devenu un Loup-garou !"
                     )

    db.session.add(loup_garou)

    room_25 = Jeu(titre="Room 25",
                  url_img="https://cdn2.philibertnet.com/432631-large_default/room-25-deuxieme-edition.jpg",
                  auteur="François Rouzé",
                  editeur="Matagot",
                  annee_edition=2012,
                  nb_min_joueurs=1,
                  nb_max_joueurs=6,
                  duree_moyenne=30,
                  type_jeu="Modulable, coopération, négociation",
                  complexite="Expert",
                  age_min=10,
                  resume="Dans ce jeu télévisé futuriste, vous jouez votre vie, pris au piège dans un complexe de 25 salles aux effets souvent indésirables ! Coopérez les uns avec les autres pour trouver la sortie : la Room 25. Soyez prudents, parmi vous se cache peut-être un gardien, qui n’hésitera pas à employer les méthodes les plus extrêmes pour vous empêcher de sortir."
                  )

    db.session.add(room_25)
    db.session.commit()
    
    # --------------------------> JOUEURS <--------------------------#

    thomas = Joueur(pseudo="Thomo",
                    id = 1,
                    mdp=generate_password_hash("thomo", method='sha256'),
                    nom="Nicolosi",
                    prenom="Thomas",
                    avatar=" https://www.mantes-actu.net/wp-content/uploads/2020/12/Capture-d%E2%80%99e%CC%81cran-2020-12-13-a%CC%80-14.17.22.jpeg",
                    email="thomoledozo@gmail.com",
                    notif_mail=True,
                    mode_vacances=False,
                    est_tuteur=True,
                    est_resp_groupe=True,
                    jeux_proposes = [loup_garou],
                    admin = True,)
    db.session.add(thomas)

    remy = Joueur(pseudo="Rems",
                  id = 2,
                  mdp=generate_password_hash("rems", method='sha256'),
                  nom="Remy",
                  prenom="Nicolo",
                  avatar=" https://lh3.googleusercontent.com/proxy/6CgdG47MdsUbPXJ8RHR0Oq5CKVARK7YPXsk47fNfSyp5EWuN8xuTbJI3JhE5Sefs0mOduXXdyQEFGeZOTe1z2vlXSMJBb1WXa5r38cLcskKkpotsqZNR9hbOg1WGJkz5HzE",
                  email="remszoo@gmail.com",
                  notif_mail=True,
                  mode_vacances=False,
                  est_tuteur=True,
                  est_resp_groupe=True,
                  admin = True,)

    db.session.add(remy)

    axel = Joueur(pseudo="Axelou",
                  id = 3,
                  mdp=generate_password_hash("axel", method='sha256'),
                  nom="lexA",
                  prenom="Axel",
                  avatar=" https://cdn-europe1.lanmedia.fr/var/europe1/storage/images/europe1/culture/jeux-video-fortnite-depasse-les-250-millions-de-joueurs-et-se-tourne-vers-sa-coupe-du-monde-3881045/52667327-1-fre-FR/Jeux-video-Fortnite-depasse-les-250-millions-de-joueurs-et-se-tourne-vers-sa-Coupe-du-monde.jpg",
                  email="BDmaster@gmail.com",
                  notif_mail=True,
                  mode_vacances=False,
                  est_tuteur=True,
                  est_resp_groupe=True,
                  admin = False,)

    db.session.add(axel)

    dylan = Joueur(pseudo="Rkioma",
                   id = 4,
                   mdp=generate_password_hash("nul", method='sha256'),
                   nom="dozo",
                   prenom="Dylen",
                   avatar=" https://staticr1.blastingcdn.com/media/photogallery/2020/9/29/660x290/b_1200x680/di-meco-chambre-dimitri-payet-sur-son-poids-les-supporters-de-lom-en-feu-sur-twitter_2522648.jpg",
                   email="grosbg@gmail.com",
                   notif_mail=True,
                   mode_vacances=False,
                   est_tuteur=True,
                   est_resp_groupe=True,
                   admin = False,)

    db.session.add(dylan)
    # --------------------------> CRENEAUX <--------------------------#
    crenau1 = Creneau(id_creneau=1,
                      jour_creneau=1,
                      heure_deb_creneau=17,
                      heure_fin_creneau=19,
                      ecart_dispo=1)
    db.session.add(crenau1)
    crenau2 = Creneau(id_creneau=2,
                      jour_creneau=3,
                      heure_deb_creneau=15,
                      heure_fin_creneau=19,
                      ecart_dispo=2)
    db.session.add(crenau2)
    crenau3 = Creneau(id_creneau=3,
                      jour_creneau=4,
                      heure_deb_creneau=15,
                      heure_fin_creneau=19,
                      ecart_dispo=2)
    db.session.add(crenau3)

    # --------------------------> SESSIONS <--------------------------#

    session1 = Session(id_session=1,
                       jour_session=datetime.date(2021, 1, 15),
                       heure_deb_session=12,
                       nb_parties_prevues=2,
                       etat_session=0,
                       adresse_id=1,
                       participants=[])
    db.session.add(session1)

    session2 = Session(id_session=2,
                       jour_session=datetime.date(2021, 2, 20),
                       heure_deb_session=15,
                       nb_parties_prevues=4,
                       etat_session=0,
                       adresse_id=2,
                       participants=[])
    db.session.add(session2)

    session3 = Session(id_session=3,
                       jour_session=datetime.date(2021, 2, 21),
                       heure_deb_session=10,
                       nb_parties_prevues=1,
                       etat_session=0,
                       adresse_id=3,
                       participants=[])
    db.session.add(session3)

    # --------------------------> ADRESSES <--------------------------#

    adresse1 = Adresse(id_adresse=1,
                       nom_adresse=" 5Clos Des Ouches",
                       nom_ville="Coinces",
                       code_postal=45310,
                       joueurs_habitant=[thomas, remy],
                       sessions_ayant_lieu=[session1])
    db.session.add(adresse1)
    adresse2 = Adresse(id_adresse=2,
                       nom_adresse="35 Boulevard Du Pastaga",
                       nom_ville="Marseille",
                       code_postal=13000,
                       joueurs_habitant=[axel])
    db.session.add(adresse2)

    adresse3 = Adresse(id_adresse=3,
                       nom_adresse="27 Rue De Kaaris",
                       nom_ville="Sevran",
                       code_postal=93071,
                       joueurs_habitant=[dylan],
                       sessions_ayant_lieu=[session2])
    db.session.add(adresse3)

    # --------------------------> MESSAGES <--------------------------#

    message1 = Message(id_message=1,
                       date_message=datetime.date(2021, 1, 10),
                       contenu_message="Salut Mike le plus bo",
                       joueur_emmeteur=thomas,
                       joueurs_recepteur=[axel])
    db.session.add(message1)

    message2 = Message(id_message=2,
                       date_message=datetime.date(2021, 2, 10),
                       contenu_message="Salut Thomo",
                       joueur_emmeteur=axel,
                       joueurs_recepteur=[thomas])
    db.session.add(message2)

    message3 = Message(id_message=3,
                       date_message=datetime.date(2021, 2, 10),
                       contenu_message="Salut Rems",
                       joueur_emmeteur=axel,
                       joueurs_recepteur=[remy])
    db.session.add(message2)

    message4 = Message(id_message=4,
                       date_message=datetime.date(2021, 3, 10),
                       contenu_message="Salut Axelou",
                       joueur_emmeteur=remy,
                       joueurs_recepteur=[axel])
    db.session.add(message4)

    db.session.commit()

    

    # --------------------------> AJOUT DES RELATIONS <--------------------------#

    # ------ createur_session -----#
    session1.createur_pseudo = thomas.pseudo
    session2.createur_pseudo = remy.pseudo
    session3.createur_pseudo = dylan.pseudo

    # ------ adresse session -----#
    session1.adresse_id = thomas.adresse.id_adresse
    session2.adresse_id = axel.adresse.id_adresse
    session3.adresse_id = dylan.adresse.id_adresse

    #------ jeu session -----#
    session1.titre_jeu = loup_garou.titre
    session2.titre_jeu = room_25.titre
    session3.titre_jeu = terraforming_mars.titre

    #------ participants session -----#
    db.session.commit()
    session1.participants.append(remy)
    session2.participants.extend([thomas]) 
    session2.participants.extend([axel])
    session3.participants.extend([dylan])

    # ------ jeux possedés -----#
    thomas.jeux_possedes.append(room_25)
    thomas.jeux_possedes.append(loup_garou)
    dylan.jeux_possedes.append(terraforming_mars)

    
    db.session.commit()
