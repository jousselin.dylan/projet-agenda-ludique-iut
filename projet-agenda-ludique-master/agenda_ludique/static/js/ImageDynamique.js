changeImg = function(input) {
    if (input.value) {
        $('.avatar-bg').css({
            'background':'url('+input.value+')',
            'background-size':'cover',
            'background-position': '50% 50%'
        });
    }
    else {
        $('.avatar-bg').css({
            'background':'url(https://www.gravatar.com/avatar/1234566?size=200&d=mm)',
            'background-size':'cover',
            'background-position': '50% 50%'
        });
    }
};

changeImgGame = function(input) {
    if (input.value) {
        $('#img_jeu').attr('src',input.value);
    }
    else {
        $('#img_jeu').attr('src',"/static/img/150x150.png");
    }
};

changeImgProfil = function(input) {
    if (input.value) {
        $('#img_profil').attr('src',input.value);
    }
    else {
        $('#img_profil').attr('src',"https://www.gravatar.com/avatar/1234566?size=200&d=mm");
    }
};