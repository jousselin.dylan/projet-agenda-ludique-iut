function initMap() {
    const myLatLng = { lat: 47.902964, lng: 1.909251 };
    const map = new google.maps.Map(document.getElementById("carte-google"), {
        zoom: 10,
        center: myLatLng,
    });
   
    for (var i = 0; i < markers.length; i++) {
        var latitude = markers[i]["lat"];
        var longitude = markers[i]["lng"];
        var coord = { lat: latitude, lng: longitude };
        new google.maps.Marker({
            position: coord,
            map,
            title: "a",
        });
    }

}
