import os.path
from flask import Flask
import flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))


app = Flask(__name__, static_url_path='/static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SECRET_KEY'] = "de046956-23b4-44fe-a848-34205aeb3274"
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'sqlite:///'+mkpath('./donnees.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
# login_manager = LoginManager(app)
# login_manager.init_app(app)
# login_manager.login_view = "login
from .models import Joueur, ORM
from .auth import auth as auth_blueprint
app.register_blueprint(auth_blueprint)
from .views import views as views_blueprint
app.register_blueprint(views_blueprint)

login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)
@login_manager.user_loader
def load_user(id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return ORM.find_by_id(id)