from .models import ORM, Jeu, lat_long_via_adress
from agenda_ludique.app import app, db
from flask import render_template, redirect, url_for, Blueprint, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, login_required, logout_user, current_user
import calendar
from datetime import timedelta
from datetime import date
from flask_wtf import FlaskForm
from wtforms.fields.html5 import DateField
from wtforms import validators, SubmitField

views = Blueprint('views', __name__)

class dateForm(FlaskForm):
    date = DateField('Sélectionner date', format='%Y-%m-%d')
    valider = SubmitField("Valider")

@app.route("/", methods=['GET','POST'])
def home():
    form = dateForm()
    if form.validate_on_submit():
        today = form.date.data
    else:
        today = date.today()
    liste_jeux=ORM.liste_jeux()
    num_semaine = today.isocalendar()[1]
    start = today - timedelta(days=today.weekday())
    end = start + timedelta(days=6)
    semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    sessions_par_jour = dict()
    for jours in semaine:
        sessions_par_jour[jours] = ORM.session_par_jours(jours,num_semaine)
    return render_template(
        "home.html",
        title="Coffre à jeux",
        la_liste=liste_jeux,
        semaine=semaine,
        sessions_par_jour=sessions_par_jour,
        week_start=start.strftime("%d/%m/%Y"),
        week_end=end.strftime("%d/%m/%Y"),
        form=form
    )


# --------------------------------------> FONCTIONS POUR LA CONNEXION <--------------------------------------#


@app.route("/inscription/")
def register():
    return render_template(
        "register.html",
        title="Inscription"
    )


@app.route("/login/")
def login():
    return render_template(
        "login.html",
        title="Connexion"
    )


@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("home"))


# --------------------------------------> FONCTIONS POUR LE PROFIL <--------------------------------------#


@app.route("/profil/")
def getProfil():
    user = ORM.get_joueur(current_user.pseudo)
    listJeuUser = ORM.get_jeu_par_joueur(current_user.pseudo)
    return render_template(
        "profil.html",
        title="Mon profil",
        joueur=user,
        liste_jeux=listJeuUser
    )

@app.route("/profil/<string:pseudo>")
def getProfilByPseudo(pseudo):
    user = ORM.get_joueur(pseudo)
    listJeuUser = ORM.get_jeu_par_joueur(pseudo)
    return render_template(
        "profil.html",
        title=pseudo,
        joueur=user,
        liste_jeux=listJeuUser
    )
    
@app.route("/profil/delete")
def deleteProfil():
    return "Page pas encore créée", 404


# --------------------------------------> FONCTIONS POUR LES EVENTS <--------------------------------------#


@app.route("/events/")
def getEvents():
    sessions = ORM.liste_session()
    markers = list()
    for session in sessions:
        adresse = str(session.adresse.nom_adresse) + \
            ", " + str(session.adresse.code_postal) + \
            ", " + str(session.adresse.nom_ville)
        markers.append(lat_long_via_adress(adresse))
    return render_template(
        "events.html",
        title="Evenements",
        evenements=sessions,
        markers=markers
    )


@app.route("/events/create")
def createEvent():
    return render_template(
        "createEvent.html",
        title="Créer un évènement"
    )


@app.route("/events/<int:id>/delete")
def deleteEvent(id):
    return "Page pas encore créée", 404


@app.route("/events/<int:id>/join")
def joinEvent(id):
    return "Page pas encore créée", 404


# --------------------------------------> FONCTIONS POUR LES GROUPES<--------------------------------------#


@app.route("/groups/create")
def createGroup():
    return render_template(
        "createGroup.html",
        title="Créer un groupe"
    )


@app.route("/groups/<int:id>/delete")
def deleteGroup(id):
    return "Page pas encore créée", 404


# --------------------------------------> FONCTIONS POUR LA GALERIE <--------------------------------------#


@app.route("/galerie/")
def galerie():
    return render_template(
        "galerie.html",
        title="Galerie",
        sessions=ORM.liste_session()
    )


# --------------------------------------> FONCTIONS POUR LA CARTE<--------------------------------------#


@app.route("/carte/")
def carte():
    sessions = ORM.liste_session()
    markers = list()
    for session in sessions:
        adresse = str(session.adresse.nom_adresse) + \
            ", " + str(session.adresse.code_postal) + \
            ", " + str(session.adresse.nom_ville)
        markers.append(lat_long_via_adress(adresse))
    return render_template(
        "carte.html",
        title="Carte des soirées",
        evenements=sessions,
        markers=markers
    )


# --------------------------------------> FONCTIONS POUR LES JEUX <--------------------------------------#


@app.route("/games/<string:titre>")
def getPageGame(titre):
    return render_template(
        "pageGame.html",
        titre=titre,
        title=ORM.get_jeu(titre),
        jeux=ORM.get_jeu(titre)
    )


@app.route("/games/propose")
@login_required
def proposeGame():
    return render_template(
        "proposeGame.html",
        title="Proposer un jeu"
    )


@app.route("/games/propose/commit", methods=['POST'])
def proposeGame_post():
    titre = request.form.get('titre')
    jeu = ORM.get_jeu(titre)
    if jeu:
        flash("Jeu déjà présent dans la base de données.")
        return redirect(url_for("proposeGame"))
    url_img = request.form.get('img')
    auteur = request.form.get('auteur')
    editeur = request.form.get('editeur')
    annee_edition = request.form.get('anneeEdit')
    nb_min_joueurs = request.form.get('min')
    nb_max_joueurs = request.form.get('max')
    duree_moyenne = request.form.get('temps')
    type_jeu = request.form.get('type')
    complexite = request.form.get('complexite')
    age_min = request.form.get('age_min')
    resume = request.form.get('resume')
    new_game = Jeu(titre=titre, url_img=url_img, auteur=auteur, editeur=editeur, annee_edition=annee_edition,
                   nb_min_joueurs=nb_min_joueurs,
                   nb_max_joueurs=nb_max_joueurs, duree_moyenne=duree_moyenne, type_jeu=type_jeu, complexite=complexite,
                   age_min=age_min, resume=resume)
    db.session.add(new_game)
    db.session.commit()
    url = "games/" + titre
    print('--------')
    print(url)
    print('--------')
    return redirect(url_for("getPageGame", titre=titre))


@app.route("/games/search", methods=['POST', 'GET'])
def search():
    liste_jeux = []
    recherche = ""
    type_recherche = ""
    liste_jeux = ORM.liste_jeux()
    if request.method == "POST":
        recherche = request.form.get('research')
        type_recherche = request.form.get('type_research')
        if type_recherche == "titre":
            liste_jeux = ORM.recherche_titre(recherche)
        elif type_recherche == "auteur":
            liste_jeux = ORM.recherche_auteur(recherche)
        elif type_recherche == "nombre minimum de joueurs":
            liste_jeux = ORM.recherche_nb_min_joueurs(recherche)
        elif type_recherche == "nombre maximum de joueurs":
            liste_jeux = ORM.recherche_nb_max_joueurs(recherche)
        elif type_recherche == "duree moyenne":
            liste_jeux = ORM.recherche_duree_moyenne(recherche)
        elif type_recherche == "type":
            liste_jeux = ORM.recherche_type_jeu(recherche)
        elif type_recherche == "complexite":
            liste_jeux = ORM.recherche_complexite(recherche)
        else:
            liste_jeux = ORM.recherche_age_min(recherche)
    return render_template(
        "home.html",
        title="Coffre à jeux",
        type_recherche=type_recherche,
        recherche=recherche,
        la_liste=liste_jeux)


@app.route("/games/tri", methods=['POST', 'GET'])
def tri():
    type_tri = ""
    liste_jeux = ORM.liste_jeux()
    if request.method == "POST":
        type_tri = request.form.get('type_tri')
        if type_tri == "auteur":
            liste_jeux = ORM.tri_auteur()
        elif type_tri == "complexite":
            liste_jeux = ORM.tri_complexite()
        elif type_tri == "age_min":
            liste_jeux = ORM.tri_age_min()
        elif type_tri == "type":
            liste_jeux = ORM.tri_type_jeu()
        elif type_tri == "duree_moyenne":
            liste_jeux = ORM.tri_duree_moyenne()
        elif type_tri == "nombre_maximum_de_joueurs":
            liste_jeux = ORM.tri_nb_max_joueurs()
        else:
            liste_jeux == ORM.tri_nb_min_joueurs()

    return render_template(
        "home.html",
        title="Coffre à jeux",
        type_tri=type_tri,
        la_liste=liste_jeux)


# --------------------------------------> FONCTIONS POUR LES ADMINS <--------------------------------------#
@app.route("/games/proposes")
@login_required
def jeux_proposes():
    jeux_propose = ORM.get_jeux_proposes()
    print('______________')
    print(jeux_propose)
    jeux = []
    joueurs = []
    for elem in jeux_propose:
        jeu_titre = elem[1]
        jeux.append(ORM.get_jeu(elem[1]))
        joueurs.append(elem[0])
    if current_user.admin:
        return render_template(
            "game_proposes.html",
            title = "Jeux à valider",
            jeux= jeux,
            joueurs = joueurs,
            len = len)
    else:
        flash("Vous n'avez pas accès à cette page")
        return redirect(url_for("home"))
    
@app.route("/gererUtilisateurs")
@login_required
def gerer_utilisateurs():
    liste_joueurs=ORM.liste_joueurs()
    return render_template(
        "gerer_utilisateurs.html",
        title = "Gérer les utilisateurs",
        la_liste = liste_joueurs
        )

@app.route("/user/search", methods=['POST', 'GET'])
def search_user():
    liste_joueurs = []
    recherche = ""
    if request.method == "POST":
        recherche = request.form.get('research')
        liste_joueurs = ORM.recherche_user(recherche)
    return render_template(
        "gerer_utilisateurs.html",
        recherche=recherche,
        la_liste=liste_joueurs)
    
@app.route("/gererUtilisateurs/<string:pseudo>/changerStatut")
def changerStatut(pseudo):
    user = ORM.find_pseudo(pseudo)
    print("----------------------------------")
    print(user)
    print("----------------------------------")
    if (user.admin):
        user.admin = False
    else:
        user.admin = True
    db.session.commit()
    return redirect(url_for("gerer_utilisateurs")) 

# @app.route("/amis/add",methods=('GET', 'POST'))
# def ajouter_ami(pseudo):
#     if request.method == 'POST':
#         db.session.add(association_joueur_joueur(joueur_courant,get_joueur(pseudo))) #faire méthode pour récuperer le joueur connecter
#         db.session.commit()

# @app.route("/amis/delete",methods=('GET', 'POST'))
# def supprimer_ami(pseudo):
#     if request.method == 'POST':
#         relation=get_relation(joueur_courant,pseudo)
#             db.session.delete(relation)
#             db.session.commit()
#
# @app.route("groupe/creer")
# def creer_groupe(nom):
#     db.session.add(Groupe(Groupe.nom_groupe(nom))
#     db.session.add(Joueur.est_resp_groupe(True))
#     db.session.commit()
