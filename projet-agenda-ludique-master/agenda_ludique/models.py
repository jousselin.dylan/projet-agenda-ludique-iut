from .app import db
from flask_login import UserMixin
from sqlalchemy.sql import select
import requests
import datetime
import calendar
from datetime import timedelta
import locale
locale.setlocale(locale.LC_TIME, '')


class Joueur(UserMixin, db.Model):  # many to many avec Créneau, et avec lui même
    pseudo = db.Column(db.String(20), primary_key=True)
    id = db.Column(db.Integer, unique=True)
    mdp = db.Column(db.String(150))
    nom = db.Column(db.String(40))
    prenom = db.Column(db.String(40))
    avatar = db.Column(db.String(100))
    email = db.Column(db.String(60))
    notif_mail = db.Column(db.Boolean)
    mode_vacances = db.Column(db.Boolean)
    est_tuteur = db.Column(db.Boolean)
    est_resp_groupe = db.Column(db.Boolean)
    admin = db.Column(db.Boolean)
    creneaux = db.relationship("Creneau",
                               secondary="etre_libre",
                               backref="joueurs")
    joueurs_masques = db.relationship("Joueur",
                                      secondary="masquer",
                                      backref="masquer_par_joueur")
    groupes = db.relationship("Groupe",
                              secondary="faire_partie",
                              backref="joueurs")
    message_envoyes = db.relationship("Message", backref='joueur_emmeteur')
    adresse_id = db.Column(db.Integer, db.ForeignKey('adresse.id_adresse'))
    groupes = db.relationship("Groupe",
                              secondary="faire_partie",
                              backref="joueurs")
    amis = db.relationship("Joueur",
                           secondary="etre_amis",
                           backref="joueurs")
    messages_reçus = db.relationship("Message",
                                     secondary="recevoir",
                                     backref="joueurs_recepteur")
    jeux_preferes = db.relationship("Jeu",
                                    secondary="preferer",
                                    backref="joueurs_preferer")
    jeux_possedes = db.relationship("Jeu",
                                    secondary="posseder",
                                    backref="joueurs_posseder")
    jeux_proposes = db.relationship("Jeu",
                                    secondary="jeux_proposes",
                                    backref="jeux_proposes")
    creer_session = db.relationship("Session", backref=db.backref(
        "createur_session", uselist=None), foreign_keys='Session.createur_pseudo')  # one to one avec session
    id_session_participe = db.Column(
        db.Integer, db.ForeignKey("session.id_session"))
    photos_participe = db.relationship("PhotoGalerie",
                             secondary="photo_participants",
                             backref="joueurs_ayant_participe")

    def __repr__(self):
        return "<Joueur: pseudo (%s) nom %s prenom: %s>\n" % (self.pseudo, self.nom, self.prenom)


class Creneau(db.Model):
    id_creneau = db.Column(db.Integer, primary_key=True)
    jour_creneau = db.Column(db.Integer)
    heure_deb_creneau = db.Column(db.Integer)
    heure_fin_creneau = db.Column(db.Integer)
    ecart_dispo = db.Column(db.Integer)


class Session(db.Model):  # one to many avec Adresse
    id_session = db.Column(db.Integer, primary_key=True)
    jour_session = db.Column(db.Date)
    heure_deb_session = db.Column(db.Integer)
    nb_parties_prevues = db.Column(db.Integer)
    etat_session = db.Column(db.Integer)
    adresse_id = db.Column(db.Integer, db.ForeignKey('adresse.id_adresse'))
    titre_jeu = db.Column(db.String(50), db.ForeignKey('jeu.titre'))
    createur_pseudo = db.Column(db.String(20), db.ForeignKey("joueur.pseudo"))
    participants = db.relationship(
        "Joueur", backref="participe_session", foreign_keys='Joueur.id_session_participe')


class Adresse(db.Model):
    id_adresse = db.Column(db.Integer, primary_key=True)
    nom_adresse = db.Column(db.String(100))
    complement = db.Column(db.String(100))
    nom_ville = db.Column(db.String(50))
    code_postal = db.Column(db.Integer)
    joueurs_habitant = db.relationship("Joueur", backref='adresse')
    sessions_ayant_lieu = db.relationship("Session", backref='adresse')


class Jeu(db.Model):
    titre = db.Column(db.String(50), primary_key=True)
    url_img = db.Column(db.String(1000))
    auteur = db.Column(db.String(50))
    editeur = db.Column(db.String(50))
    annee_edition = db.Column(db.Integer)
    nb_min_joueurs = db.Column(db.Integer)
    nb_max_joueurs = db.Column(db.Integer)
    duree_moyenne = db.Column(db.Integer)
    type_jeu = db.Column(db.String(50))
    complexite = db.Column(db.String(50))
    age_min = db.Column(db.Integer)
    resume = db.Column(db.String(3000))
    sessions = db.relationship("Session", backref="jeu_session")


class Message(db.Model):
    id_message = db.Column(db.Integer, primary_key=True)
    date_message = db.Column(db.Date)
    contenu_message = db.Column(db.String(1000))
    pseudo_joueur_emmeteur = db.Column(
        db.String(20), db.ForeignKey('joueur.pseudo'))


class Groupe(db.Model):
    nom_groupe = db.Column(db.String(50), primary_key=True)

class PhotoGalerie(db.Model):
    id_photo_galerie = db.Column(db.Integer,primary_key=True)
    titre_photo_galerie = db.Column(db.String(50))
    date_photo_galerie = db.Column(db.Date)


association_joueur_creneau = db.Table('etre_libre', db.metadata,
                                      db.Column('joueur_pseudo', db.String(20),
                                                db.ForeignKey('joueur.pseudo')),
                                      db.Column('creneau_id', db.Integer,
                                                db.ForeignKey('creneau.id_creneau'))
                                      )

association_joueur_joueur = db.Table('masquer', db.metadata,
                                     db.Column('joueur_pseudo', db.String(20),
                                               db.ForeignKey('joueur.pseudo')),
                                     db.Column('joueur_pseudo', db.String(20),
                                               db.ForeignKey('joueur.pseudo'))
                                     )

association_faire_partie = db.Table('faire_partie', db.metadata,
                                    db.Column('joueur_pseudo', db.String(20),
                                              db.ForeignKey('joueur.pseudo')),
                                    db.Column('groupe_nom', db.String(20),
                                              db.ForeignKey('groupe.nom_groupe'))
                                    )

association_etre_amis = db.Table('etre_amis', db.metadata,
                                 db.Column('joueur_pseudo', db.String(20),
                                           db.ForeignKey('joueur.pseudo')),
                                 db.Column('joueur_pseudo', db.String(20),
                                           db.ForeignKey('joueur.pseudo'))
                                 )


association_recevoir = db.Table('recevoir', db.metadata,
                                db.Column('joueur_pseudo', db.String(20),
                                          db.ForeignKey('joueur.pseudo')),
                                db.Column('id_message', db.Integer,
                                          db.ForeignKey('message.id_message'))
                                )

association_preferer = db.Table('preferer', db.metadata,
                                db.Column('joueur_pseudo', db.String(20),
                                          db.ForeignKey('joueur.pseudo')),
                                db.Column('titre_jeux', db.Integer,
                                          db.ForeignKey('jeu.titre'))
                                )

association_posseder = db.Table('posseder', db.metadata,
                                db.Column('joueur_pseudo', db.String(20),
                                          db.ForeignKey('joueur.pseudo')),
                                db.Column('titre_jeux', db.Integer,
                                          db.ForeignKey('jeu.titre'))
                                )

association_savoir_expliquer = db.Table('Savoir_expliquer', db.metadata,
                                        db.Column('joueur_pseudo', db.String(20),
                                                  db.ForeignKey('joueur.pseudo')),
                                        db.Column('titre_jeux', db.Integer,
                                                  db.ForeignKey('jeu.titre'))
                                        )
jeux_expliques = db.relationship("Jeux_expliques",
                                 secondary="expliquer",
                                 backref="joueurs")

association_proposer_jeux = db.Table('jeux_proposes', db.metadata,
                                     db.Column('joueur_propose_pseudo', db.String(20),
                                               db.ForeignKey('joueur.pseudo')),
                                     db.Column('titre_jeu', db.Integer,
                                               db.ForeignKey('jeu.titre'))
                                     )

association_photo_participants = db.Table('photo_participants', db.metadata,
                                      db.Column('joueur_pseudo', db.String(20),
                                                db.ForeignKey('joueur.pseudo')),
                                      db.Column('id_photo_galerie', db.Integer,
                                                db.ForeignKey(PhotoGalerie.id_photo_galerie))
                                    )

class ORM:

    #----------------------------------------------> AMIS <----------------------------------------------#

    @staticmethod
    def liste_joueur():
        return Joueur.query.all()

    @staticmethod
    def get_joueur(pseudo):
        return Joueur.query.filter(Joueur.pseudo == pseudo).one()

    @staticmethod
    def get_relation(joueur_courant, pseudo):
        return association_joueur_joueur.query.filter_by(association_joueur_joueur.joueur_pseudo == joueur_courant,
                                                         association_joueur_joueur.joueur_pseudo2 == pseudo).one()

    @staticmethod
    def liste_ami(joueur_courant):
        return association_joueur_joueur.query.filter_by(association_joueur_joueur.joueur_pseudo == joueur_courant)

    #----------------------------------------------> JEUX <----------------------------------------------#

    @staticmethod
    def liste_jeux():
        return Jeu.query.all()

    @staticmethod
    def get_jeu(titre):
        return Jeu.query.filter_by(titre=titre).first()

    @staticmethod
    def get_jeu_par_auteur(auteur):
        return Jeu.query.filter_by(Jeu.auteur == auteur).all()

    @staticmethod
    def get_jeu_par_session(session):
        return Jeu.query.filter_by(Jeu.titre == session.titre_jeu).all()

    @staticmethod
    def get_jeu_par_nb_min_joueur(nb):
        return Jeu.query.filter_by(Jeu.nb_min_joueurs == nb).all()

    @staticmethod
    def get_jeu_par_nb_max_joueur(nb):
        return Jeu.query.filter_by(Jeu.nb_max_joueurs == nb).all()

    @staticmethod
    def get_jeu_par_duree_moyenne(duree):
        return Jeu.query.filter_by(Jeu.duree_moyenne == duree).all()

    @staticmethod
    def get_jeu_par_type_jeu(type_jeu):
        return Jeu.query.filter_by(Jeu.type_jeu == type_jeu).all()

    @staticmethod
    def get_jeu_par_complexite(complexite):
        return Jeu.query.filter_by(Jeu.complexite == complexite).all()

    @staticmethod
    def get_jeu_par_age_min(age):
        return Jeu.query.filter_by(Jeu.age_min == age).all()

    @staticmethod
    def get_jeux_proposes():
        return db.session.query(association_proposer_jeux).all()

    @staticmethod 
    def get_jeu_par_joueur(pseudo):
        return Joueur.query.with_entities(Joueur.jeux_possedes).filter(Joueur.pseudo == pseudo).all()

    #----------------------------------------------> RECHERCHE <----------------------------------------------#

    @staticmethod
    def recherche_titre(recherche):
        return Jeu.query.filter(Jeu.titre.contains(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_auteur(recherche):
        return Jeu.query.filter(Jeu.auteur.contains(recherche)).order_by(Jeu.auteur.asc()).all()

    @staticmethod
    def recherche_nb_min_joueurs(recherche):
        return Jeu.query.filter(Jeu.nb_min_joueurs >= int(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_nb_max_joueurs(recherche):
        return Jeu.query.filter(Jeu.nb_max_joueurs <= int(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_duree_moyenne(recherche):
        return Jeu.query.filter(Jeu.duree_moyenne <= int(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_type_jeu(recherche):
        return Jeu.query.filter(Jeu.type_jeu.contains(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_complexite(recherche):
        return Jeu.query.filter(Jeu.complexite.contains(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_age_min(recherche):
        return Jeu.query.filter(Jeu.age_min <= int(recherche)).order_by(Jeu.titre.asc()).all()

    @staticmethod
    def recherche_user(recherche):
        return Joueur.query.filter(Joueur.pseudo.startswith(recherche)).order_by(Joueur.pseudo.asc()).all()

    # ----------------------------------------------> TRI <----------------------------------------------#

    @staticmethod
    def tri_auteur():
        return Jeu.query.order_by(Jeu.auteur.asc())

    @staticmethod
    def tri_complexite():
        return Jeu.query.order_by(Jeu.complexite.asc())

    @staticmethod
    def tri_age_min():
        return Jeu.query.order_by(Jeu.age_min)

    @staticmethod
    def tri_nb_min_joueurs():
        return Jeu.query.order_by(Jeu.nb_min_joueurs)

    @staticmethod
    def tri_nb_max_joueurs():
        return Jeu.query.order_by(Jeu.nb_max_joueurs)

    @staticmethod
    def tri_duree_moyenne():
        return Jeu.query.order_by(Jeu.duree_moyenne)

    @staticmethod
    def tri_type_jeu():
        return Jeu.query.order_by(Jeu.type_jeu)

    #----------------------------------------------> SOIREE <----------------------------------------------#

    @staticmethod
    def liste_session():
        return Session.query.all()

    @staticmethod
    def session_par_jours(jour,num_semaine):
        jours = set(db.session.query(Session.jour_session))
        res = list()
        for day in jours:
            if(day[0].strftime("%A").lower() == jour.lower() and day[0].isocalendar()[1] == num_semaine):
                res.append(db.session.query(Session).filter(
                    Session.jour_session == day[0]).all())
        return res

#----------------------------------------------> JOUEUR <----------------------------------------------#

    @staticmethod
    def liste_joueurs():
        return Joueur.query.all()

    @staticmethod
    def find_pseudo(pseudo):
        return Joueur.query.filter_by(pseudo=pseudo).first()

    @staticmethod
    def find_by_id(id):
        return Joueur.query.filter_by(id=id).first()

    @staticmethod
    def is_admin(id):
        return Joueur.query(est_admin).filter_by(id=id)
#----------------------------------------------> ADRESSE <----------------------------------------------#

    @staticmethod
    def adresse_exist(nom, ville, code_postal, complement):
        return Adresse.query.filter_by(nom_ville=ville, nom_adresse=nom, code_postal=code_postal, complement=complement).first()


def lat_long_via_adress(address_or_zipcode):
    lat, lng = None, None
    api_key = 'AIzaSyAPy_6pciMc81Yp71xVlPSe1hPdsIhLBHo'
    base_url = "https://maps.googleapis.com/maps/api/geocode/json"
    endpoint = f"{base_url}?address={address_or_zipcode}&key={api_key}"
    # see how our endpoint includes our API key? Yes this is yet another reason to restrict the key
    r = requests.get(endpoint)
    if r.status_code not in range(200, 299):
        return None, None
    try:
        '''
        This try block incase any of our inputs are invalid. This is done instead
        of actually writing out handlers for all kinds of responses.
        '''
        results = r.json()['results'][0]
        lati = results['geometry']['location']['lat']
        lngi = results['geometry']['location']['lng']
    except:
        pass
    return {"lat": lati, "lng": lngi}
