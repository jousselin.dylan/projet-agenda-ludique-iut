from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from agenda_ludique.app import app, db
from .models import Joueur , Adresse, ORM
from flask_login import login_user, login_required, logout_user, current_user
import datetime

auth = Blueprint('auth', __name__)

@auth.route("/login/")
def login():
    return render_template(
        "login.html",
        title = "Login"
    )

@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("home"))

@auth.route("/login/", methods=['POST'])
def login_post():
    pseudo = request.form.get('pseudo')
    password = request.form.get('password')
    user = ORM.find_pseudo(pseudo)
    # regarde si le pseudo existe
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.mdp, password):
        flash('Vérifiez vos identifiants et réessayez')
        return redirect(url_for('auth.login')) # if the user doesn't exist or password is wrong, reload the page
    # if the above check passes, then we know the user has the right credentials
    print("---------------------")
    login_user(user)
    print(current_user.pseudo)
    return redirect(url_for("home"))


@auth.route("/inscription/")
def register():
    return render_template(
        "register.html",
        title = "Register"
    )

@auth.route("/inscription", methods=['POST'])
def register_post():
    pseudo = request.form.get('pseudo')
    user = ORM.find_pseudo(pseudo)
    if user:
        flash("Votre pseudo est déjà utilisé.")
        return redirect(url_for("auth.register"))
    
    id = len(Joueur.query.all())+1
    mdp = request.form.get('password')
    confirmMdp = request.form.get('confirmpass')
    if (mdp != confirmMdp):
        flash("Erreur de confirmation de mot de passe")
        return redirect(url_for("auth.register"))
    prenom = request.form.get('firstname')
    nom = request.form.get('lastname')
    email = request.form.get('email')
    if (request.form.get('avatar-file')):
        urlImg = request.form.get('avatar-file')
    else:
        urlImg = "https://www.gravatar.com/avatar/1234566?size=200&d=mm"
    ville = request.form.get('ville')
    code_postal = request.form.get('codePostale')
    adresse = request.form.get('adresse')
    complement = request.form.get('complement')
    adresseExist = ORM.adresse_exist(adresse,ville,code_postal,complement)
    if(adresseExist):
        new_adresse = adresseExist
    else:
        adresse_id = len(Adresse.query.all())+1
        new_adresse = Adresse(id_adresse = adresse_id,nom_ville = ville, code_postal = code_postal, nom_adresse = adresse, joueurs_habitant = [])
        db.session.add(new_adresse)
    new_user = Joueur(pseudo = pseudo,id=id ,mdp=generate_password_hash(mdp, method='sha256'), prenom=prenom, nom=nom, avatar=urlImg, email=email, adresse_id = adresse_id, admin = False)
    new_adresse.joueurs_habitant.append(new_user)
    db.session.add(new_user)
    db.session.commit()
    return redirect(url_for("auth.login"))


@app.route("/profil/modif")
def modifProfil():
    user = ORM.get_joueur(current_user.pseudo)
    listJeuUser = ORM.get_jeu_par_joueur(current_user.pseudo)
    return render_template(
        "profilModif.html",
        title="Mon profil",
        joueur=user,
        liste_jeux=listJeuUser
    )

@app.route("/profil/modif/commit", methods=['POST'])
def modifProfil_post():
    user = ORM.find_pseudo(current_user.pseudo)
    pseudo = request.form.get("pseudo")
    if ORM.find_pseudo(pseudo):
        if pseudo != current_user.pseudo:
            flash("Votre pseudo est déjà utilisé.")
            return redirect(url_for("modifProfil"))
    mdp = request.form.get('password')
    confirmMdp = request.form.get('confirmpass')
    if (mdp):
        if (mdp != confirmMdp):
            flash("Erreur de confirmation de mot de passe")
            return redirect(url_for("modifProfil"))
        else:
            user.mdp = generate_password_hash(mdp,method='sha256')
    user.pseudo = pseudo
    user.avatar = request.form.get("avatar")
    user.prenom = request.form.get("prenom")
    user.nom = request.form.get("nom")
    user.email = request.form.get("mail")
    user.adresse.nom_adresse = request.form.get("adresse")
    user.adresse.nom_ville = request.form.get("ville")
    if (request.form.get("vacances") == "oui"):
        user.mode_vacances = True
    else:
        user.mode_vacances = False
    db.session.commit()
    flash("Votre profil a été modifié.")
    return redirect(url_for("modifProfil"))